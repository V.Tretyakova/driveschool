<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'category' => $faker->regexify('[A-Z]{2}'),
        'title' => $faker->sentence,
        'subscription' => $faker->sentences(6,true),
        'documents' => $faker->sentences(10,true),
        'age' => $faker->numberBetween(18,25),
        'price' => $faker->numberBetween(5000,15000),
        'duration' => $faker->numberBetween(20,40),
        'disabled' => 'on',
    ];
});
