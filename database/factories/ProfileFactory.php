<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Profile;
use Faker\Generator as Faker;

$factory->define(Profile::class, function (Faker $faker) {
    return [
        'surname' => $faker->name,
        'name' => $faker->firstName,
        'patronymic' => $faker->name,
        'image' => $faker->imageUrl(),
        'birthday' => $faker->date('Y-m-d'),
        'birthplace' => $faker->city,
        'address' => $faker->address,
        'passport_series' => Str::random(2),
        'passport_number' => $faker->numberBetween(1, 999999),
        'passport_issued' => $faker->company,
        'passport_date' => $faker->date('Y-m-d'),
        'phones' => ['mobile' => ['0955946752','0721172620'], 'static' => ['0642626145']],
    ];
});
