<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Driver;
use App\Order;
use App\Service;
use App\User;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'user_id' => factory(User::class)->create()->id,
        'service_id' => factory(Service::class)->create()->id,
        'driver_id' => factory(Driver::class)->create()->id,
        'receipt' => $faker->imageUrl(),
        'reserved' => $faker->datetime(),
        'confirmed' => null,
    ];
});
