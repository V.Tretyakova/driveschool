<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDriversTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('drivers', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->text('surname');
            $table->text('name');
            $table->text('patronymic')->nullable();

            $table->date('birthday');
            $table->text('birthplace');

            $table->text('address');

            $table->text('passport_series');
            $table->text('passport_number');
            $table->text('passport_issued');
            $table->date('passport_date');
            $table->jsonb('phones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('drivers');
    }
}
