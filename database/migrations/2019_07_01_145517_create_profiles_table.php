<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('surname')->nullable();
            $table->text('name')->nullable();
            $table->text('patronymic')->nullable();
            $table->string('image')->nullable();

            $table->date('birthday')->nullable();
            $table->text('birthplace')->nullable();

            $table->text('address')->nullable();

            $table->text('passport_series')->nullable();
            $table->text('passport_number')->nullable();
            $table->text('passport_issued')->nullable();
            $table->date('passport_date')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
