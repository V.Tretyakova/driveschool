<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ProfilesTableSeeder::class);
        $this->call(ServiceTableSeeder::class);
        $this->call(DriverTableSeeder::class);
        $this->call(OrderTableSeeder::class);
    }

}
