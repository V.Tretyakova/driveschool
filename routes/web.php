<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify'=> true]);

Route::get('/', 'HomeController@index')->name('index')->middleware('verified');
Route::get('lang/{locale}', 'HomeController@lang');

Route::view('requirement','requirement')->name('requirement');

Route::view('price','price')->name('price');

Route::view('contact','contact')->name('contact');

Route::view('gallery','gallery')->name('gallery');

Route::resource('profile', 'ProfileController');

Route::get('dashboard','DashboardController@index')->name('dashboard.index');

Route::resource('service', 'ServiceController');

Route::resource('driver', 'DriverController');

Route::resource('order', 'OrderController');
