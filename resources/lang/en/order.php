<?php
return [
    'order' => 'Order',
    'save' => 'Save order',
    'category_id' => 'Category',
    'receipt' => 'Receipt',
    'choose_file' => 'Choose file'
];
