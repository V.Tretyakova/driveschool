<?php
return [
    'card' => 'Driver card',
    'surname' => 'Last name',
    'name' => 'First name',
    'patronymic' => 'Middle name',
    'birthday' => 'Birthday',
    'birthplace' => 'Place of birth',
    'address' => 'Living address',
    'passport_data' => 'Passport',
    'passport_series' => 'Passport series',
    'passport_number' => 'Passport number',
    'passport_issued' => 'Issued',
    'passport_date' => 'Issue date',
    'save' => 'Save card',
    'phones' => 'Contact phones',
    'mobile_phone' => 'Mobile',
    'static_phone' => 'Static'
];
