<?php
return [
    'service' => 'Service',
    'category' => 'Category',
    'title' => 'Title',
    'subscription' => 'Subscription',
    'documents' => 'Documents',
    'age' => 'Age limit',
    'price' => 'Price',
    'duration' => 'Course duration',
    'disabled' => 'Disabled',
    'save' => 'Save service',
    'choose_category' => 'Choose category',
];
