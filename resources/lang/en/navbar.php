<?php

return [
    'main' => 'Main',
    'requirement' => 'Requirement',
    'price' => 'Price',
    'contact' => 'Contact',
    'gallery' => 'Gallery',
    'login' => 'Login',
    'logout' => 'Logout',
    'register' => 'Register',
    'admin_panel' => 'Administer',
    'profile' => 'Profile',
    'language' => 'Language',
    'request' => 'Enroll'
];
