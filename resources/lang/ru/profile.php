<?php
return [
    'profile' => 'Профиль пользователя',
    'choose_file' => 'Выберите файл',
    'surname' => 'Фамилия',
    'name' => 'Имя',
    'patronymic' => 'Отчество',
    'birthday' => 'Дата рождения',
    'birthplace' => 'Место рождения',
    'address' => 'Место проживания',
    'passport_data' => 'Паспортные данные',
    'passport_series' => 'Серия',
    'passport_number' => 'Номер',
    'passport_issued' => 'Выдан',
    'passport_date' => 'Дата выдачи',
    'save' => 'Сохранить профиль',
    'phones' => 'Телефоны для связи',
    'mobile_phone' => 'Мобильный',
    'static_phone' => 'Городской'
];
