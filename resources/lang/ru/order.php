<?php
return [
    'order' => 'Запись на вождение',
    'save' => 'Сохранить запись',
    'category_id' => 'Категория',
    'receipt' => 'Квитанция',
    'choose_file' => 'Выберите файл'
];
