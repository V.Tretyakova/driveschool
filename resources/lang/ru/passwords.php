<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Пароль должен содержать не менее восьми символов и соответствовать паролю из поля подтверждения.',
    'reset' => 'Ваш пароль сброшен!',
    'sent' => 'Ссылка для сброса пароля отправлена на Ваш почтовый ящик!',
    'token' => 'Пароль сброса недействителен.',
    'user' => "Пользователь с таким почтовым адресом не найден.",

];
