<?php

return [
    'main' => 'Главная',
    'requirement' => 'Обучение',
    'price' => 'Цены',
    'contact' => 'Контакты',
    'gallery' => 'Галерея',
    'login' => 'Войти',
    'logout' => 'Выйти',
    'register' => 'Регистрация',
    'admin_panel' => 'Управление',
    'profile' => 'Профиль',
    'language' => 'Язык',
    'request' => 'Записаться'
];
