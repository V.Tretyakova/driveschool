<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Учетные данные не найдены',
    'throttle' => 'Превышен лимит попыток входа. Повторите попытку через :seconds секунд.',
    'email' => 'E-mail',
    'password' => 'Пароль',
    'remember' => 'Запомнить',
    'forgot' => 'Забыли пароль?',
    'login' => 'Войти',
    'register' => 'Регистрация',
    'name' => 'Имя пользователя',
    'confirm' => 'Подтвердите пароль',
    'phone' => 'Контактный телефон',
];
