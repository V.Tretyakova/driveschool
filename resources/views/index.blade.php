@extends('layouts.app')

<script type="text/javascript" src="{{asset('js/docsTable.js')}}"></script>

@section('content')
    <div class="container card">
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/full_main_picture.png')}}" alt="full_main_picture">
            </div>
        </div>
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/phones.jpg')}}" alt="phones">
            </div>
        </div>
        <div class="pt-1 row justify-content-center bg-blue">
            <div class="col">
                <h3 class="text-center text-white font-weight-bold">МЫ НЕ ЧАСТНАЯ АВТОШКОЛА!</h3>
            </div>
        </div>
        <div class="pt-1 row justify-content-center bg-blue">
            <div class="col">
                <h5 class="text-center text-white font-weight-bold">Почему у нас в итоге ДЕШЕВЛЕ, БЫСТРЕЕ и, главное,
                    КАЧЕСТВЕННЕЕ - смотрите далее по сайту!</h5>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h1 class="pt-2 text-center font-weight-bold">О школе</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-7 col-lg-7 col-xl-7 col-7 ">
                <h5 class="pt-2 text-center font-weight-bold">ЧЕМ МЫ ОТЛИЧАЕМСЯ ОТ ДРУГИХ</h5>
                <ul>
                    <li class="font-weight-bold">МЫ НЕ ЧАСТНАЯ АВТОШКОЛА;</li>
                    <li>лучшие результаты сдачи экзаменов по Правилам и вождению <strong>с первого раза</strong> в
                        ГИБДД;
                    </li>
                    <li>работают преподаватели высшей категории по специально разработанным методикам обучения;</li>
                    <li>практическому вождению обучают мастера-психологи;</li>
                    <li class="font-weight-bold">АВТОДРОМ ВО ДВОРЕ АВТОШКОЛЫ;</li>
                    <li>лучшая база подготовки в ЛНР!</li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-5 col-lg-5 col-xl-5 col-5">
                <h5 class="pt-2 text-center font-weight-bold">ОБУЧЕНИЕ ВОЖДЕНИЮ</h5>
                <iframe width="100%" height="250" src="https://www.youtube.com/embed/s5WU28lz_4A" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-7">
                <h5 class="pt-2 text-center font-weight-bold">УЧЕБНАЯ БАЗА</h5>
                <ul>Для качественной подготовки водителей Луганская автошкола ДОСААФ имеет <strong>СОБСТВЕННУЮ
                        материально-техническую базу</strong>, включающую:
                    <li>оборудованные классы теоретической подготовки;</li>
                    <li>технические комплексы обучения (компьютеры, проекторы, аудиосистемы, проекционные экраны);</li>
                    <li>тренажеры первоначального обучения навыкам вождения;</li>
                    <li>оборудованные учебные транспортные средства;</li>
                    <li>площадка первоначального обучения вождению;</li>
                    <li>маневровая площадка обучения вождению легковых автомобилей (автодром) на расстоянии 30 м от
                        учебного корпуса;
                    </li>
                    <li>маневровая площадка обучения вождению грузовых автомобилей (автодром).</li>
                </ul>
            </div>
            <div class="col-5">
                <img width="100%" src="{{asset('img/stop_overtake.jpg')}}" alt="stop_overtake">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-12">
                <p><span class="tab"> Луганская автошкола ДОСААФ ЛНР <strong>(Луганская автошкола Добровольного
                            Общества Содействия Армии, Авиации и Флоту Луганской Народной Республики)</strong> ведет
                        свою «родословную» от Луганского автомотоклуба ДОСААФ УССР, который был создан
                        <strong>в 1956 году.</strong> В советское время автошкола признавалась лучшей на Украине и
                        была награждена Красным Знаменем.</span>
                </p>
                <p><span class="tab">За 60 лет существования автошкола подготовила десятки тысяч водителей всех
                        категорий - от мотоцикла до грузового автомобиля с прицепом. Наиболее массовая и качественная
                        подготовка велась для нужд Вооруженных Сил СССР. После прекращения существования СССР акценты
                        работы сместились в сторону подготовки водителей легковых автомобилей (категория «В»).</span>
                </p>
                <p> <span class="tab">Авторитет Автошколы ДОСААФ сформирован десятилетиями добросовестного труда,
                        прежде всего, преподавательского состава организации. <strong>ДОСААФ это традиции,
                        </strong> профессионализм и качество подготовки. Наш коллектив понимает, что традиции
                        <strong>порядочного, честного и участливого отношения к ученикам</strong> – единственная
                        гарантия успешного существования автошколы. Без этого не помогут ни компьютеры, ни «модные»
                        учебные авто. Мы дорожим своей репутацией! Именно поэтому Вы к нам придете учиться, справедливо
                        полагая, что <strong>Луганская автошкола ДОСААФ это ФИРМА с «человеческим лицом»</strong>,
                        где с Вами работают профессионалы, уважающие человеческое достоинство, которые <strong>терпеливо
                            будут Вас учить,</strong> передавая свои знания и умения, делясь опытом и навыками.</span>
                </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-7 col-lg-7 col-md-8">
                <h5 class="pt-2 text-center font-weight-bold">Методические материалы</h5>
                <table class="table table-responsive">
                    <tr>
                        <td>
                            <img height="60%" width="60%" src="{{asset('img/admin_penalty.jpg')}}" alt="penalty" class="img-thumbnail img-fluid">
                        </td>
                        <td>
                            Административные наказания за правонарушения в сфере дорожного движения ЛНР
                        </td>
                        <td>
                            <select name="select_penalties" id="select_penalties" class="btn btn-info">
                                <option value="https://drive.google.com/uc?authuser=0&id=11vzZcQPFspWm11qyvUuVQIsOVixSxoNm&export=download">.pdf</option>
                                <option value="https://drive.google.com/uc?authuser=0&id=1gruvRy11OtwmzhGZ1K6Wo10V-y6XjGw-&export=download">.docx</option>
                                <option value="https://drive.google.com/uc?authuser=0&id=1gruvRy11OtwmzhGZ1K6Wo10V-y6XjGw-&export=download">.html</option>
                            </select>
                        </td>
                        <td>
                            <button name="download_penalties" id="download_penalties" class="btn btn-success">Скачать</button>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <img height="60%" width="60%" src="{{asset('img/pdd.jpg')}}" alt="pdd" class="img-thumbnail">
                        </td>
                        <td>
                            Правила дорожного движения ЛНР
                        </td>
                        <td>
                            <select name="select_pdd" id="select_pdd" class="btn btn-info">
                                <option value="https://drive.google.com/uc?authuser=0&id=19x0UHlUIouyzMR4DuqLWGrC74XGvSUCS&export=download">.pdf</option>
                                <option value="https://drive.google.com/uc?id=16VJ1-E9Z09mE3GVIBEgG7yK5SmyFF7MJ&authuser=0&export=download">.docx</option>
                                <option value="https://drive.google.com/uc?id=1l8345PgjwF2UFM_MvKWjuLSwr6hTliAt&authuser=0&export=download">.fb2</option>
                            </select>
                        </td>
                        <td>
                            <button name="download_pdd" id="download_pdd" class="btn btn-success">Скачать</button>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-xl-5 col-lg-5 col-md-4">
                <img width="100%" src="{{asset('img/transmission.jpg')}}" alt="transmission">
            </div>
        </div>
    </div>
@endsection
