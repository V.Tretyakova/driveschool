@extends('layouts.app')

@section('content')
    <div class="container card">
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/full_requirements_picture.jpeg')}}" alt="full_main_picture">
            </div>
        </div>
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/phones.jpg')}}" alt="phones">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h1 class="pt-2 text-center font-weight-bold">Условия обучения</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-7 col-lg-7 col-xl-7 col-7 ">
                <h5 class="pt-2 text-center font-weight-bold">Время обучения</h5>
                <p><span class="tab"><strong>Теоретические занятия</strong> для слушателей курсов подготовки водителей кат.
                    «В» (легковые
                    автомобили) проходят в составе учебной группы <strong>в вечернее время с понедельника по
                        пятницу.</strong>
                    Продолжительность учебы – примерно <strong>10 недель (включая 40 ч практического вождения – 20
                        занятий)</strong>.
                </span></p>
                <p><span class="tab"><strong>Практическое вождение автомобиля</strong> – по индивидуальному графику,
                    <strong>В УДОБНОЕ ДЛЯ ВАС ВРЕМЯ</strong> (согласовывается Вами с инструктором) с 7.00 до 19.00,
                    <strong>с понедельника по субботу.</strong></span></p>
            </div>
            <div class="col-xs-12 col-md-5 col-lg-5 col-xl-5 col-5">
                <h5 class="pt-2 text-center font-weight-bold">ОБУЧЕНИЕ ВОЖДЕНИЮ</h5>
                <img width="100%" src="{{asset('img/blue_daewoo_parking.png')}}" alt="red_daewoo">
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-7">
                <h5 class="pt-2 text-center font-weight-bold">Документы</h5>
                <p><span class="tab"><i>Примечание: Чтобы «занять место» в своей учебной группе достаточно принести в автошколу копию
                    паспорта, номер телефона и квитанцию об оплате (полной или частичной).</i></span></p>
                <ul>
                    <li> Копия паспорта, идентификационного кода и номер телефона.</li>
                    <li>Квитанция об оплате.</li>
                    <li>4 цветных фото 3 см х 4 см (плюс еще 2 фото Вам понадобятся для медсправки).</li>
                    <li>Медицинская справка о допуске к управлению транспортным средством и две ее копии (любая
                        подходящая больница ЛНР, нарколог и психиатр).
                    </li>
                </ul>
                <p><span class="tab">После успешного окончания обучения в автошколе Вы в составе учебной группы под руководством
                    преподавателя направляетесь в МРЭО ГИБДД на сдачу экзаменов (теория и вождение).</span></p>
            </div>
            <div class="col-5">
                <img width="100%" src="{{asset('img/documents_picture.png')}}" alt="documents_picture">
            </div>
        </div>
    </div>
@endsection
