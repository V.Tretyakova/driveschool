@extends('layouts.app')
<script src="{{asset('js/yandexMap.js')}}" defer></script>
<script src="https://api-maps.yandex.ru/2.1/?apikey=70b410ae-f63d-4a22-837d-b7e65d19aa53&lang=ru_RU" type="text/javascript">
</script>
@section('content')
    <div class="container card">
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/full_contact_picture.png')}}" alt="full_contact_picture">
            </div>
        </div>
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/phones.jpg')}}" alt="phones">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h1 class="pt-2 text-center font-weight-bold">Контакты</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6 col-6 ">
                <h5 class="pt-2 text-center font-weight-bold">Адрес</h5>
                <p><span class="tab"><strong>91033, г. Луганск, ул. Оборонная, 24</strong></span></p>
                <p><i>(3-х этажное здание ОО «ДОСААФ» ЛНР между автовокзалом и торговым центром «Аврора»).</i></p>
                <h5 class="pt-2 text-center font-weight-bold">Телефоны</h5>
                <ul class="list-style-none">
                    <li><img src="{{asset('img/vodafone_50x50.png')}}" alt="vodafone"><strong>+38 (050) 024-63-44</strong></li>
                    <li><img src="{{asset('img/lugacom_50x50.png')}}" alt="lugacom"><strong>+38 (072) 135-01-59</strong></li>
                    <li><img src="{{asset('img/general_50x50.png')}}" alt="general"><strong>+38 (0642) 33-07-78</strong></li>
                </ul>
                <p><span class="tab"><strong>Записываться в автошколу на обучение лучше с 8.30 до 19.00 с понедельника по пятницу круглый год (кроме праздничных дней).</strong></span></p>
            </div>
            <div class="col-xs-12 col-md-6 col-lg-6 col-xl-6 col-6">
                <h5 class="pt-2 text-center font-weight-bold">Карта</h5>
                <div id="map" style="width: 100%; height: 400px; background-color: lightgrey">
                </div>
            </div>
        </div>
    </div>
@endsection

