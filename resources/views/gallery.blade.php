@extends('layouts.app')
<script type="text/javascript" src="{{mix('js/fresco.js')}}"></script>
<script type="text/javascript" src="{{mix('js/gallery.js')}}"></script>
<link rel="stylesheet" type="text/css" href="{{asset('css/fresco/fresco.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('css/gallery.css')}}">
@section('content')
    <div class="container card">
        <div class="row justify-content-center">
            <div class="col">
                <div class="row">
                    <div class="col">
                        <h1 class="pt-2 text-center font-weight-bold">Галерея</h1>
                    </div>
                </div>
                <div class="row py-4">
                    <span class="mx-auto d-block">
                        <img id="main_front_img" src="{{asset('img/slider/front.jpg')}}" class="mx-auto">
                    </span>
                </div>
                <div class="demonstrations">
                    <a href="{{asset('img/slider/front.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img id="front_img" src="{{asset('img/slider/front.jpg')}}" class="img-fluid">
                    </a>
                    <a href="{{asset('img/slider/autodrome.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img src="{{asset('img/slider/autodrome.jpg')}}" class="img-fluid">
                    </a>
                    <a href="{{asset('img/slider/classroom1.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img src="{{asset('img/slider/classroom1.jpg')}}" class="img-fluid">
                    </a>
                    <a href="{{asset('img/slider/classroom2.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img src="{{asset('img/slider/classroom2.jpg')}}" class="img-fluid">
                    </a>
                    <a href="{{asset('img/slider/classroom3.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img src="{{asset('img/slider/classroom3.jpg')}}" class="img-fluid">
                    </a>
                    <a href="{{asset('img/slider/classroom4.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img src="{{asset('img/slider/classroom4.jpg')}}" class="img-fluid">
                    </a>
                    <a href="{{asset('img/slider/classroom5.jpg')}}" class="fresco" data-fresco-group="gallery"
                       data-fresco-type="image">
                        <img src="{{asset('img/slider/classroom5.jpg')}}" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection
