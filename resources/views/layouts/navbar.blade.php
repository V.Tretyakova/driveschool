<nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}">
            <img src="{{ asset('img/icon_50x50.jpg')}}" width="50px" height="50px" alt="icon">
        </a>
        <a class="navbar-brand" href="{{ route('index') }}">
            <strong>{{ config('app.name', 'Laravel') }}</strong>
        </a>

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of Navbar -->
            <ul class="navbar-nav mr-auto">
                @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('dashboard.index') }}">{{__('navbar.admin_panel')}}</a>
                    </li>
                @endauth
            </ul>

            <!-- Right Side Of Navbar -->
            <ul class="navbar-nav ml-auto">

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('requirement') }}">{{ __('navbar.requirement') }}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('price') }}">{{ __('navbar.price') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('contact') }}">{{ __('navbar.contact') }}</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ route('gallery') }}">{{ __('navbar.gallery') }}</a>
                </li>

{{--                <li class="nav-item">--}}
{{--                    <a class="nav-link" href="{{ route('request') }}">{{ __('navbar.request') }}</a>--}}
{{--                </li>--}}

                <!-- Authentication Links -->
                @guest
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('login') }}">{{ __('navbar.login') }}</a>
                    </li>
                    @if (Route::has('register'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}">{{ __('navbar.register') }}</a>
                        </li>
                    @endif
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                               v-pre>{{__('navbar.language')}}
                            </a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item flag-icon flag-icon-us" href="lang/en"></a>
                                <a class="dropdown-item flag-icon flag-icon-ru" href="lang/ru"></a>
                            </div>
                        </li>
                    </ul>
                @else
                    <li class="nav-item dropdown">
                        <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                            {{ Auth::user()->name }} <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                            <li>
                                <a class="dropdown-item" href="{{ route('profile.edit', ['profile' => Auth::user()->profile->id]) }}">
                                    {{ __('navbar.profile') }}
                                </a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item dropdown-toggle" href="#">{{__('navbar.language')}}</a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item flag-icon flag-icon-us" href="/lang/en"></a></li>
                                    <li><a class="dropdown-item flag-icon flag-icon-ru" href="/lang/ru"></a></li>
                                </ul>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('navbar.logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>

                    </li>
                @endguest
            </ul>
        </div>
    </div>
</nav>
