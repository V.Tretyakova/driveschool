<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ mix('js/app.js') }}" defer></script>
    <script src="{{ mix('js/fontawesome-free/all.js') }}" defer></script>

    <script src="{{ mix('js/navbar.js') }}" defer></script>
    <script src="{{ mix('js/fileBrowser.js')}}" defer></script>
    <script src="{{ mix('js/dropdown.js')}}" defer></script>


    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/dropdown.css')}}">
</head>
<body background="{{asset('img/grey_texture_wave.jpg')}}">
<div id="app">

    @include('layouts.navbar')

    <main class="py-4">
        @yield('content')
    </main>

    @include('layouts.footer')
</div>
</body>
</html>
