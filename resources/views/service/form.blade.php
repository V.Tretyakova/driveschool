<div class="form-group row">
    <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('service.category') }}</label>

    <div class="col-md-6">
        <select name="category" id="category" class="form-control @error('category') is-invalid @enderror" required>
            <option value="" selected>{{__('service.choose_category')}}</option>
            @php
                $serviceCategories = Config::get('categories.service');
            @endphp
            @foreach($serviceCategories as $serviceCategory)
                @if( isset($service->category) && ($service->category == $serviceCategory) )
                    <option value="{{$serviceCategory}}" selected>{{$serviceCategory}}</option>
                @elseif(old('category') == $serviceCategory)
                    <option value="{{$serviceCategory}}" selected>{{$serviceCategory}}</option>
                @else
                    <option value="{{$serviceCategory}}">{{$serviceCategory}}</option>
                @endif
            @endforeach
        </select>
        @error('category')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="title" class="col-md-4 col-form-label text-md-right">{{ __('service.title') }}</label>

    <div class="col-md-6">
        <textarea name="title" id="title" cols="30" rows="3" class="form-control @error('title') is-invalid @enderror"
                  required autofocus>{{ old('title') ?? $service->title ?? '' }}</textarea>

        @error('title')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="subscription" class="col-md-4 col-form-label text-md-right">{{ __('service.subscription') }}</label>

    <div class="col-md-6">
        <textarea name="subscription" id="subscription" cols="30" rows="3"
                  class="form-control @error('subscription') is-invalid @enderror"
                  autofocus>{{ old('subscription') ?? $service->subscription ?? '' }}</textarea>

        @error('subscription')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="documents" class="col-md-4 col-form-label text-md-right">{{ __('service.documents') }}</label>

    <div class="col-md-6">
        <textarea name="documents" id="documents" cols="30" rows="10"
                  class="form-control @error('documents') is-invalid @enderror"
                  required autofocus>{{ old('documents') ?? $service->documents ?? '' }}</textarea>

        @error('documents')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="age" class="col-md-4 col-form-label text-md-right">{{ __('service.age') }}</label>

    <div class="col-md-6">
        <input id="age" type="number"
               class="form-control @error('age') is-invalid @enderror" name="age"
               value="{{ old('age') ?? $service->age ?? '' }}" required autocomplete="age" autofocus>

        @error('age')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('service.price') }}</label>

    <div class="col-md-6">
        <input id="price" type="number"
               class="form-control @error('price') is-invalid @enderror" name="price"
               value="{{ old('price') ?? $service->price ?? '' }}" required autocomplete="price" autofocus>

        @error('price')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="duration" class="col-md-4 col-form-label text-md-right">{{ __('service.duration') }}</label>

    <div class="col-md-6">
        <input id="duration" type="text"
               class="form-control @error('duration') is-invalid @enderror" name="duration"
               value="{{ old('duration') ?? $service->duration ?? '' }}" required autocomplete="duration" autofocus>

        @error('duration')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <div class="offset-4 col-md-6">
        <div class="custom-control custom-checkbox">
            <input type="hidden" id="disabled" name="disabled"
                   value="<?php (isset($service->disabled) && ($service->disabled == 'on')) ? print('on') : print('off'); ?>">
            <input type="checkbox" class="custom-control-input form-control @error('disabled') is-invalid @enderror" id="disabledCheckbox">
            @error('disabled')
            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
            <label class="custom-control-label" for="disabledCheckbox">{{ __('service.disabled') }}</label>
        </div>
    </div>
</div>

<hr>
@csrf
