@extends('layouts.app')

<script type="text/javascript" src="{{ mix('js/service/show.js') }}" defer></script>
<script type="text/javascript" src="{{mix('js/service/disabledCheckbox.js')}}" defer></script>

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('service.service') }}</div>

                    <div class="card-body">
                        <form method="GET" action="{{ route('service.show',['service' => $service->id]) }}" id="show_service_form">
                            @method('GET')
                            @include('service.form')
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('service.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
