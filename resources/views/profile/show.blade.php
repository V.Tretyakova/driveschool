@extends('layouts.app')

<script type="text/javascript" src="{{ mix('js/phoneMask.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/phoneInput.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/profile/show.js') }}"></script>

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('profile.profile') }}</div>

                    <div class="card-body">
                        <form method="GET" action="{{ route('profile.show',['profile' => $profile->id]) }}" enctype="multipart/form-data" id="show_profile_form">
                            @method('GET')
                            @include('profile.form')
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('profile.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
