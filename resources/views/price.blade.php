@extends('layouts.app')

@section('content')
    <div class="container card">
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/full_price_picture.jpg')}}" alt="full_price_picture">
            </div>
        </div>
        <div class="row justify-content-center bg-blue">
            <div class="col">
                <img width="100%" src="{{asset('img/phones.jpg')}}" alt="phones">
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h1 class="pt-2 text-center font-weight-bold">Цены</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-md-7 col-lg-7 col-xl-7 col-7 ">
                <h5 class="pt-2 text-center font-weight-bold">Цена и форма оплаты</h5>
                <p><span class="tab"><strong>Оплату</strong> за обучение можно производить в любом Госбанке ЛНР либо целиком, либо <strong>двумя частями</strong> – первую половину суммы перед началом обучения, вторую половину суммы примерно через месяц после начала обучения.
                </span></p>
                <p><span class="tab"><strong>Оплата за горюче-смазочные материалы</strong> – при прохождении практической подготовки по вождению автомобиля.</span></p>
                <ul>Виды лицензированной в ГИБДД МВД ЛНР подготовки:
                    <li>Подготовка водителей категории <strong>«В»</strong> - легковые автомобили – <strong>11 000 р.</strong></li>
                    <li>Переподготовка водителей, имеющих кат. <strong>«С» на кат. «В»</strong> (20 часов практического вождения на легковом автомобиле) – <strong>3500 р.</strong></li>
                </ul>
            </div>
            <div class="col-xs-12 col-md-5 col-lg-5 col-xl-5 col-5">
                <h5 class="pt-2 text-center font-weight-bold">ОБУЧЕНИЕ ВОЖДЕНИЮ</h5>
                <img width="100%" src="{{asset('img/red_daewoo_parking.png')}}" alt="red_daewoo_parking">
                <strong>ВЫБИРАЙТЕ АВТОШКОЛУ, ГДЕ УЧАТ, А НЕ ТОЛЬКО БЕРУТ ДЕНЬГИ.</strong>
            </div>
        </div>
    </div>
@endsection
