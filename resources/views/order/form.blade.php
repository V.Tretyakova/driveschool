<div class="form-group row">
    <label for="category" class="col-md-4 col-form-label text-md-right">{{ __('service.category') }}</label>

    <div class="col-md-6">
        <select name="service_id" id="service_id" class="form-control @error('service_id') is-invalid @enderror" required>
            <option value="" selected>{{__('service.choose_category')}}</option>
            @foreach($services as $service)
                @if(isset($order->service) && ($order->service_id == $service->id) )
                    <option value="{{$service->id}}" selected>{{$service->category}} - {{$service->title}}</option>
                @elseif(old('service_id') == $service->id)
                    <option value="{{$service->id}}" selected>{{$service->category}} - {{$service->title}}</option>
                @else
                    <option value="{{$service->id}}">{{$service->category}} - {{$service->title}}</option>
                @endif
            @endforeach
        </select>
        @error('service_id')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

@include('driver.form')

<hr>

<div class="form-group row align-items-end">
    <div class="col-md-4 col-form-label text-md-right">
        <img width="100px" height="100px"
             src="{{ (isset($order->receipt) && (!is_null($order->receipt))) ? asset('storage/' . $order->receipt) : '' }}"
             alt="Image" onclick="event.preventDefault();
                                                     document.getElementById('image').click();">
    </div>
    <div class="col-md-6">
        <div class="custom-file">
            <input id="image" type="file"
                   class="custom-file-input @error('receipt') is-invalid @enderror" name="receipt">
            <label for="receipt" class="custom-file-label">{{__('order.choose_file')}}</label>
            @error('receipt')
            <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
            @enderror
        </div>
    </div>
</div>
@csrf
