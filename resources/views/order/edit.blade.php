@extends('layouts.app')

<script type="text/javascript" src="{{ mix('js/phoneMask.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/phoneInput.js') }}"></script>

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('order.order') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('order.update',['order' => $order->id]) }}" enctype="multipart/form-data">
                            @method('PUT')
                            @include('order.form')
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('order.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
