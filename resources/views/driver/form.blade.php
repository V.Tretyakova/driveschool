<div class="form-group row">
    <label for="surname" class="col-md-4 col-form-label text-md-right">{{ __('driver.surname') }}</label>

    <div class="col-md-6">
        <input id="surname" type="text"
               class="form-control @error('surname') is-invalid @enderror" name="surname"
               value="{{ old('surname') ?? $driver->surname ?? $profile->surname ?? '' }}" required autofocus>

        @error('surname')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('driver.name') }}</label>

    <div class="col-md-6">
        <input id="name" type="text"
               class="form-control @error('name') is-invalid @enderror" name="name"
               value="{{ old('name') ?? $driver->name ?? $profile->name ?? '' }}" required autofocus>

        @error('name')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="patronymic" class="col-md-4 col-form-label text-md-right">{{ __('driver.patronymic') }}</label>

    <div class="col-md-6">
        <input id="name" type="text"
               class="form-control @error('patronymic') is-invalid @enderror" name="patronymic"
               value="{{ old('patronymic') ?? $driver->patronymic ?? $profile->patronymic ?? '' }}" autofocus>

        @error('patronymic')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

@php
    $phones = null;
    if(isset($driver) && !empty($driver))
        $phones = $driver->phones;
    elseif(isset($profile) && !empty($profile))
        $phones = $profile->phones;
@endphp
<div class="form-group row">
    <label for="phones" class="col-md-4 col-form-label text-md-right">{{ __('driver.mobile_phone') }}</label>
    <div class="col-md-6 phone_inputs" id="mobile_inputs">
            @if(isset($phones['mobile']))
                @for($i =0; $i < count($phones['mobile']); $i++)
                    @if($i > 0)
                        <div class="input-group">
                            <input id="phones" type="text"
                                   class="form-control @error('phones') is-invalid @enderror mobile_phone"
                                   name="phones[mobile][]"
                                   value="{{ old('phones[mobile]['.$i.']') ?? $phones['mobile'][$i] ?? '' }}"
                                   required autofocus aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary delete_phone_input_btn" type="button"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    @else
                        <div class="input-group">
                            <input id="phones" type="text"
                                   class="form-control @error('phones') is-invalid @enderror mobile_phone"
                                   name="phones[mobile][]"
                                   value="{{ old('phones[mobile]['.$i.']') ?? $phones['mobile'][$i] ?? '' }}"
                                   required autofocus aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="add_mobile_phone_btn"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    @endif
                @endfor
            @else
                <div class="input-group">
                    <input id="phones" type="text"
                           class="form-control @error('phones') is-invalid @enderror mobile_phone"
                           name="phones[mobile][]"
                           value="{{ old('phones[mobile][]') ?? '' }}" required autofocus
                           aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="add_mobile_phone_btn"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
            @endif

        @error('phones')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="phones" class="col-md-4 col-form-label text-md-right">{{ __('driver.static_phone') }}</label>

    <div class="col-md-6 phone_inputs" id="static_inputs">
            @if(isset($phones['static']))
                @for($i=0; $i < count($phones['static']); $i++)
                    @if($i > 0)
                        <div class="input-group">
                            <input id="phones" type="text"
                                   class="form-control @error('phones') is-invalid @enderror static_phone"
                                   name="phones[static][]"
                                   value="{{ old('phones[static]['.$i.']') ?? $phones['static'][$i] ?? '' }}"
                                   autofocus aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary delete_phone_input_btn" type="button"><i
                                        class="fa fa-minus"></i></button>
                            </div>
                        </div>
                    @else
                        <div class="input-group">
                            <input id="phones" type="text"
                                   class="form-control @error('phones') is-invalid @enderror static_phone"
                                   name="phones[static][]"
                                   value="{{ old('phones[static]['.$i.']') ?? $phones['static'][$i] ?? '' }}"
                                   autofocus aria-describedby="basic-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="add_static_phone_btn"><i
                                        class="fa fa-plus"></i></button>
                            </div>
                        </div>
                    @endif
                @endfor
            @else
                <div class="input-group">
                    <input id="phones" type="text"
                           class="form-control @error('phones') is-invalid @enderror static_phone"
                           name="phones[static][]"
                           value="{{ old('phones[static][]') ?? '' }}" autofocus aria-describedby="basic-addon2">
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="button" id="add_static_phone_btn"><i
                                class="fa fa-plus"></i></button>
                    </div>
                </div>
            @endif
    </div>
</div>

<div class="form-group row">
    <label for="birthday" class="col-md-4 col-form-label text-md-right">{{ __('driver.birthday') }}</label>

    <div class="col-md-6">
        <input id="birthday" type="date"
               class="form-control @error('birthday') is-invalid @enderror" name="birthday"
               value="{{ old('birthday') ?? $driver->birthday ?? $profile->birthday  ?? ''}}" required autofocus>

        @error('birthday')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="birthplace" class="col-md-4 col-form-label text-md-right">{{ __('driver.birthplace') }}</label>

    <div class="col-md-6">
        <input id="birthplace" type="text"
               class="form-control @error('birthplace') is-invalid @enderror" name="birthplace"
               value="{{ old('birthplace') ?? $driver->birthplace ?? $profile->birthplace ?? '' }}" required autofocus>

        @error('birthplace')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<div class="form-group row">
    <label for="address" class="col-md-4 col-form-label text-md-right">{{ __('driver.address') }}</label>

    <div class="col-md-6">
        <input id="address" type="text"
               class="form-control @error('address') is-invalid @enderror" name="address"
               value="{{ old('address') ?? $driver->address ?? $profile->address ?? '' }}" required autofocus>

        @error('address')
        <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
        @enderror
    </div>
</div>

<hr>

<div class="card">
    <div class="card-header">
        {{ __('driver.passport_data') }}
    </div>
    <div class="card-body">

        <div class="form-group row">
            <label for="passport_series"
                   class="col-md-4 col-form-label text-md-right">{{ __('driver.passport_series') }}</label>

            <div class="col-md-6">
                <input id="passport_series" type="text"
                       class="form-control @error('passport_series') is-invalid @enderror" name="passport_series"
                       value="{{ old('passport_series') ?? $driver->passport_series ?? $profile->passport_series ?? '' }}"
                       required
                       autofocus>

                @error('passport_series')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="passport_number"
                   class="col-md-4 col-form-label text-md-right">{{ __('driver.passport_number') }}</label>

            <div class="col-md-6">
                <input id="passport_number" type="text"
                       class="form-control @error('passport_number') is-invalid @enderror" name="passport_number"
                       value="{{ old('passport_number') ?? $driver->passport_number ?? $profile->passport_number ?? '' }}"
                       required
                       autofocus>

                @error('passport_number')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="passport_issued"
                   class="col-md-4 col-form-label text-md-right">{{ __('driver.passport_issued') }}</label>

            <div class="col-md-6">
                <input id="passport_issued" type="text"
                       class="form-control @error('passport_issued') is-invalid @enderror" name="passport_issued"
                       value="{{ old('passport_issued') ?? $driver->passport_issued ?? $profile->passport_issued ?? '' }}"
                       required
                       autofocus>

                @error('passport_issued')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>
        </div>

        <div class="form-group row">
            <label for="passport_date"
                   class="col-md-4 col-form-label text-md-right">{{ __('driver.passport_date') }}</label>

            <div class="col-md-6">
                <input id="passport_date" type="date"
                       class="form-control @error('passport_date') is-invalid @enderror" name="passport_date"
                       value="{{ old('passport_date') ?? $driver->passport_date ?? $profile->passport_date  ?? ''}}"
                       required
                       autofocus>

                @error('passport_date')
                <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
                @enderror
            </div>
        </div>
    </div>
</div>

<hr>
@csrf
