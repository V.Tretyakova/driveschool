@extends('layouts.app')

<script type="text/javascript" src="{{ mix('js/phoneMask.js') }}"></script>
<script type="text/javascript" src="{{ mix('js/phoneInput.js') }}"></script>

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('driver.card') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('driver.store') }}" enctype="multipart/form-data">
                            @include('driver.form')
                            <div class="form-group row mb-0">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('driver.save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
