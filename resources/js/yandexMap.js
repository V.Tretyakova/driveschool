ymaps.ready(init);

function init() {
    // Создание карты.
    var myMap = new ymaps.Map("map", {
        // Координаты центра карты.
        // Порядок по умолчанию: «широта, долгота».
        // Чтобы не определять координаты центра карты вручную,
        // воспользуйтесь инструментом Определение координат.
        center: [48.547924, 39.331305],
        // Уровень масштабирования. Допустимые значения:
        // от 0 (весь мир) до 19.
        zoom: 17
    });
    var driveSchool = new ymaps.Placemark([48.547924, 39.331305], {
        hintContent: 'Автошкола ДОСААФ',
        iconContent: 'Автошкола ДОСААФ'
    }, {
        preset: 'islands#redStretchyIcon'
    });

    var driveZone = new ymaps.Placemark([48.547705, 39.333132], {
        hintContent: 'Автодром ДОСААФ',
        iconContent: 'Автодром ДОСААФ'
    }, {
        preset: 'islands#blueStretchyIcon'
    });
// Размещение геообъекта на карте.
    myMap.geoObjects.add(driveSchool);
    myMap.geoObjects.add(driveZone);
}
