jQuery(function ($) {
    initPhoneMask();
    initDeletePhoneBtn();
    $('#add_mobile_phone_btn').on('click', function () {
        if (getPhoneCount() < 5) {
            $('#mobile_inputs').append('<div class="input-group"><input id="phones" type="text"\n' +
                '                               class="form-control mobile_phone"\n' +
                '                               name="phones[mobile][]" required\n' +
                '                               autofocus aria-describedby="basic-addon2">' +
                '                               <div class="input-group-append">\n' +
                '                               <button class="btn btn-outline-secondary delete_phone_input_btn" ' +
                '                               type="button"><i class="fa fa-minus"></i></button></div></div>');

            initPhoneMask();
            initDeletePhoneBtn();
        }
    });
    $('#add_static_phone_btn').on('click', function () {
        if (getPhoneCount() < 7) {
            $('#static_inputs').append('<div class="input-group"><input id="phones" type="text"\n' +
                '                               class="form-control static_phone"\n' +
                '                               name="phones[static][]" required\n' +
                '                               autofocus aria-describedby="basic-addon2">' +
                '                               <div class="input-group-append">\n' +
                '                               <button class="btn btn-outline-secondary delete_phone_input_btn" ' +
                '                                   type="button"><i class="fa fa-minus"></i></button></div></div>');
            initPhoneMask();
            initDeletePhoneBtn();
        }
    });
});

window.initDeletePhoneBtn = function () {
    $('.delete_phone_input_btn').on('click', function () {
        $(this).parent().parent().remove();
    });
};

window.getPhoneCount = function () {
    return $('.phone_inputs').children().length;
};
