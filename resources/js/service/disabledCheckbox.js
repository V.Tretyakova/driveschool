jQuery(function ($) {

    $(document).ready(function(){
        if ($("#disabled").val() == 'on') {
            $("#disabledCheckbox").attr('checked',true);
        } else {
            $("#disabledCheckbox").attr('checked',false);
        }
    });

    $("#disabledCheckbox").on("click", function () {
        if ($(this).is(":checked")) {
            $("#disabled").val('on');
        } else {
            $("#disabled").val('off');
        }
    })
});
