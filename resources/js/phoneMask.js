jQuery(function ($) {
    initPhoneMask();
});

window.initPhoneMask = function(){
    let mobile_mask = new Inputmask("+38 (999) 999-99-99");
    mobile_mask.mask($('.mobile_phone'));

    let static_mask = new Inputmask("+38 (999[9]) 9[9]-99-99");
    static_mask.mask($('.static_phone'));
};
