<?php

namespace Tests\Unit;

use App\Order;
use Illuminate\Http\UploadedFile;
use Image;
use Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @see ProfileController::storeImage()
     */
    public function testStoreImage()
    {
        $order = factory(Order::class)->create();
        $default = $order->receipt;
        $request = new Request();
        $request::offsetSet('receipt', UploadedFile::fake()->image('avatar.jpg'));

        $this->assertArrayHasKey('receipt', $request::all());

        $order->update([
            'receipt' => $request::get('receipt')->store('uploads/receipt', 'public'),
        ]);

        $this->assertNotEquals($default, Order::first()->receipt);

        Image::make(public_path('storage/' . $order->receipt))
            ->fit(300, 300, null, 'center')
            ->save();

        $this->assertFileExists(public_path('storage/' . $order->receipt));
    }
}
