<?php

namespace Tests\Unit;

use App\Http\Controllers\ProfileController;
use App\Profile;
use Illuminate\Http\UploadedFile;
use Image;
use Request;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @see ProfileController::storeImage()
     */
    public function testStoreImage()
    {
        $profile = factory(Profile::class)->create();
        $default = $profile->image;
        $request = new Request();
        $request::offsetSet('image', UploadedFile::fake()->image('avatar.jpg'));

        $this->assertArrayHasKey('image', $request::all());

        $profile->update([
            'image' => $request::get('image')->store('uploads', 'public'),
        ]);

        $this->assertNotEquals($default, Profile::first()->image);

        Image::make(public_path('storage/' . $profile->image))
            ->fit(300, 300, null, 'center')
            ->save();

        $this->assertFileExists(public_path('storage/' . $profile->image));
    }
}
