<?php

namespace Tests\Feature;

use App\Http\Controllers\ProfileController;
use App\Profile;
use App\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProfileControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @see ProfileController::index()
     */
    public function testIndex()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('profile.index'))
            ->assertOk();
    }

    /**
     * @see ProfileController::create()
     */
    public function testCreate()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('profile.create'))
            ->assertOk();
    }

    /**
     * @see ProfileController::store()
     */
    public function testStore()
    {
        $image = UploadedFile::fake()->image('avatars.jpg', 640, 480);
        $requestData = factory(Profile::class)
            ->create(['image' => new UploadedFile($image->getPath() . '\\' . $image->getFilename(), $image->name, 'image', null, true)])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store'), $requestData)
            ->assertSessionHasNoErrors();
    }

    /**
     * @see ProfileController::show()
     */
    public function testShow()
    {
        $this->actingAs($user = factory(User::class)->create())
            ->get(route('profile.show', ['profile' => $user->profile]))
            ->assertOk()
            ->assertViewHas('profile');
    }

    /**
     * @see ProfileController::edit()
     */
    public function testEdit()
    {
        $this->actingAs($user = factory(User::class)->create())
            ->get(route('profile.edit', ['profile' => $user->profile]))
            ->assertOk()
            ->assertViewHas('profile');
    }

    /**
     * @see ProfileController::update()
     */
    public function testUpdate()
    {
        $image = UploadedFile::fake()->image('avatars.jpg', 640, 480);
        $user = factory(User::class)->create();
        $requestData = factory(Profile::class)
            ->make(['image' => new UploadedFile($image->getPath() . '\\' . $image->getFilename(), $image->name, 'image', null, true)])
            ->toArray();

        $this->actingAs($user)->patch(route('profile.update', ['profile' => $user->profile]), $requestData)
            ->assertRedirect(route('profile.edit', ['profile' => $user->profile]));

        $this->assertEquals($requestData['surname'], User::first()->profile->surname);

        $this->assertEquals(User::first()->profile_id, Profile::first()->id);

    }

    /**
     * @see ProfileController::destroy()
     */
    public function testDestroy()
    {
        $this->actingAs($user = factory(User::class)->create())
            ->delete(route('profile.destroy', ['profile' => $user->profile]))
            ->assertRedirect(route('index'));

        $this->assertCount(0, Profile::all());
    }

    /** @test */
    public function testSurnameValidation()
    {
        $request = factory(Profile::class)
            ->create(['surname' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('surname');

        $request['surname'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('surname');

    }

    /** @test */
    public function testNameValidation()
    {
        $request = factory(Profile::class)
            ->create(['name' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('name');

        $request['name'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('name');
    }

    /** @test */
    public function testPatronymicValidation()
    {
        $request = factory(Profile::class)
            ->create(['patronymic' => ''])
            ->toArray();

        unset($request['image']);

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasNoErrors();

        $request['patronymic'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('patronymic');
    }

    /** @test */
    public function testImageValidation()
    {
        $request = factory(Profile::class)
            ->create()
            ->toArray();

        unset($request['image']);

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasNoErrors();

        $request['image'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('image');

        $request['image'] = UploadedFile::fake()->create('document.docx');

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('image');

        $image = UploadedFile::fake()->image('avatar.jpg', 640,480);
        $request['image'] = new UploadedFile($image->getRealPath(), $image->getFilename(), 'image', null, true);

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasNoErrors();

    }

    /** @test */
    public function testBirthdayValidation()
    {
        $request = factory(Profile::class)
            ->create(['birthday' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('birthday');
    }

    /** @test */
    public function testBirthplaceValidation()
    {
        $request = factory(Profile::class)
            ->create(['birthplace' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('birthplace');
    }

    /** @test */
    public function testAddressValidation()
    {
        $request = factory(Profile::class)
            ->create(['address' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('address');
    }

    /** @test */
    public function testPassportSeriesValidation()
    {
        $request = factory(Profile::class)
            ->create(['passport_series' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('passport_series');
    }

    /** @test */
    public function testPassportNumberValidation()
    {
        $request = factory(Profile::class)
            ->create(['passport_number' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('passport_number');

        $request['passport_number'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('passport_number');
    }

    /** @test */
    public function testPassportIssuedValidation()
    {
        $request = factory(Profile::class)
            ->create(['passport_issued' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('passport_issued');
    }

    /** @test */
    public function testPassportDateValidation()
    {
        $request = factory(Profile::class)
            ->create(['passport_date' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('passport_date');
    }

    /** @test */
    public function testPhonesValidation()
    {
        $request = factory(Profile::class)
            ->create(['phones' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('phones');

        $request['phones'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('profile.store', $request))
            ->assertSessionHasErrors('phones');
    }
}
