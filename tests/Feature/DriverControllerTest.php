<?php

namespace Tests\Feature;

use App\Driver;
use App\Http\Controllers\DriverController;
use App\Profile;
use App\User;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DriverControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @see DriverController::index()
     */
    public function testIndex()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('driver.index'))
            ->assertOk();
    }

    /**
     * @see DriverController::create()
     */
    public function testCreate()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('driver.create'))
            ->assertOk()
            ->assertViewHas(compact(['driver','profile']));
    }

    /**
     * @see DriverController::store()
     */
    public function testStore()
    {
        $requestData = factory(Profile::class)
            ->create()
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store'), $requestData)
            ->assertSessionHasNoErrors();
    }

    /**
     * @see DriverController::show()
     */
    public function testShow()
    {
        $driver = factory(Driver::class)->create();

        $this->actingAs($user = factory(User::class)->create())
            ->get(route('driver.show',['driver' => $driver]))
            ->assertOk()
            ->assertViewHas(compact('driver'));
    }

    /**
     * @see DriverController::edit()
     */
    public function testEdit()
    {
        $driver = factory(Driver::class)->create();

        $this->actingAs($user = factory(User::class)->create())
            ->get(route('driver.edit', ['driver' => $driver]))
            ->assertOk()
            ->assertViewHas(compact('driver'));
    }

    /**
     * @see DriverController::update()
     */
    public function testUpdate()
    {
        $driver = factory(Driver::class)->create();
        $requestData = factory(Profile::class)
            ->make()
            ->toArray();

        $this->actingAs(factory(User::class)->create())->patch(route('driver.update', ['driver' => $driver]), $requestData)
            ->assertRedirect(route('driver.edit', ['driver' => $driver]));

        $this->assertEquals($requestData['surname'], Driver::first()->surname);
    }

    /**
     * @see DriverController::destroy()
     */
    public function testDestroy()
    {
        $driver = factory(Driver::class)->create();

        $this->actingAs(factory(User::class)->create())
            ->delete(route('driver.destroy', ['driver' => $driver]))
            ->assertRedirect(route('index'));

        $this->assertCount(0, Driver::all());
    }

    /** @test */
    public function testSurnameValidation()
    {
        $request = factory(Driver::class)
            ->create(['surname' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('surname');

        $request['surname'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('surname');

    }

    /** @test */
    public function testNameValidation()
    {
        $request = factory(Driver::class)
            ->create(['name' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('name');

        $request['name'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('name');
    }

    /** @test */
    public function testPatronymicIsNullable()
    {
        $request = factory(Driver::class)
            ->create(['patronymic' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasNoErrors();

        $request['patronymic'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('patronymic');
    }

    /** @test */
    public function testBirthdayValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['birthday'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('birthday');
    }

    /** @test */
    public function testBirthplaceValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['birthplace'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('birthplace');
    }

    /** @test */
    public function testAddressValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['address'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('address');
    }

    /** @test */
    public function testPassportSeriesValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['passport_series'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('passport_series');
    }

    /** @test */
    public function testPassportNumberValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['passport_number'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('passport_number');

        $request['passport_number'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('passport_number');
    }

    /** @test */
    public function testPassportIssuedValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['passport_issued'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('passport_issued');
    }

    /** @test */
    public function testPassportDateValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['passport_date'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('passport_date');

        $request['passport_date'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('passport_date');
    }

    /** @test */
    public function testPhonesValidation()
    {
        $request = factory(Driver::class)
            ->create()
            ->toArray();

        $request['phones'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('phones');

        $request['phones'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('driver.store', $request))
            ->assertSessionHasErrors('phones');
    }
}
