<?php

namespace Tests\Feature;

use App\Driver;
use App\Http\Controllers\OrderController;
use App\Order;
use App\Profile;
use App\Service;
use App\User;
use Faker\Test\Provider\DateTimeTest;
use Illuminate\Http\UploadedFile;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @see OrderController::index()
     */
    public function testIndex()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('order.index'))
            ->assertOk();
    }

    /**
     * @see OrderController::create()
     */
    public function testCreate()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('order.create'))
            ->assertOk()
            ->assertViewHas(compact(['profile', 'services']));
    }

    /**
     * @see OrderController::store()
     */
    public function testStore()
    {
        $image = UploadedFile::fake()->image('receipt.jpg', 640, 480);
        $orderRequest = factory(Order::class)
            ->make(['receipt' => new UploadedFile($image->getPath() . '\\' . $image->getFilename(), $image->name, 'image', null, true)])
            ->toArray();
        unset($orderRequest['driver_id']);
        $driverRequest = factory(Driver::class)->make()->toArray();

        $this->assertCount(0, Order::all());
        $this->assertCount(1, Driver::all());

        $response = $this->actingAs(factory(User::class)->create())
            ->post(route('order.store'), array_merge($orderRequest, $driverRequest))
            ->assertSessionHasNoErrors();

        $this->assertCount(1, Order::all());
        $this->assertCount(2, Driver::all());

        $response->assertRedirect(route('order.edit', ['order' => Order::first()->id]));
    }

    /**
     * @see OrderController::show()
     */
    public function testShow()
    {
        $order = factory(Order::class)->create();
        $driver = factory(Driver::class)->create();
        $services = factory(Service::class)->create();
        $this->actingAs(factory(User::class)->create())
            ->get(route('order.show', compact(['order', 'driver', 'services'])))
            ->assertOk()
            ->assertViewHas(['order', 'driver', 'services']);
    }

    /**
     * @see OrderController::edit()
     */
    public function testEdit()
    {
        $order = factory(Order::class)->create();
        $driver = factory(Driver::class)->create();
        $services = factory(Service::class)->create();
        $this->actingAs(factory(User::class)->create())
            ->get(route('order.edit', compact(['order', 'driver', 'services'])))
            ->assertOk()
            ->assertViewHas(['order', 'driver', 'services']);
    }

    /**
     * @see OrderController::update()
     */
    public function testUpdate()
    {
        $image = UploadedFile::fake()->image('receipt.jpg', 640, 480);
        $order = factory(Order::class)
            ->create(['receipt' => new UploadedFile($image->getPath() . '\\' . $image->getFilename(), $image->name, 'image', null, true)]);

        $orderRequest = factory(Order::class)
            ->make(['receipt' => new UploadedFile($image->getPath() . '\\' . $image->getFilename(), $image->name, 'image', null, true)])
            ->toArray();
        unset($orderRequest['driver_id']);
        unset($orderRequest['user_id']);
        $driverRequest = factory(Driver::class)->make()->toArray();

        $this->assertCount(1, Order::all());
        $this->assertCount(2, Driver::all());

        $this->actingAs(factory(User::class)->create())->patch(route('order.update', ['order' => $order]), array_merge($orderRequest, $driverRequest))
            ->assertRedirect(route('order.edit', ['order' => $order]));

        $this->assertEquals($driverRequest['surname'], Order::first()->driver->surname);

        $this->assertEquals($orderRequest['service_id'], Order::first()->service_id);

    }

    /**
     * @see OrderController::destroy()
     */
    public function testDestroy()
    {
        $order = factory(Order::class)->create();
        $this->actingAs(factory(User::class)->create())
            ->delete(route('order.destroy', compact('order')))
            ->assertRedirect(route('index'));

        $this->assertCount(0, Order::all());
    }


    /** @test */
    public function testServiceIdValidation()
    {
        $request = factory(Order::class)
            ->create()
            ->toArray();

        unset($request['service_id']);

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('service_id');

        $request['service_id'] = null;

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('service_id');
    }

    /** @test */
    public function testReceiptValidation()
    {
        $request = factory(Order::class)
            ->create()
            ->toArray();

        unset($request['receipt']);

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('receipt');

        $request['receipt'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('receipt');

        $request['receipt'] = UploadedFile::fake()->create('document.docx');

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('receipt');
    }

    /** @test */
    public function testReservedValidation()
    {
        $request = factory(Order::class)
            ->create()
            ->toArray();

        unset($request['receipt']);

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('reserved');
    }

    /** @test */
    public function testConfirmedValidation()
    {
        $request = factory(Order::class)
            ->create()
            ->toArray();

        $request['confirmed'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('order.store', $request))
            ->assertSessionHasErrors('confirmed');
    }

}
