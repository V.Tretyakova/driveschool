<?php

namespace Tests\Feature;

use App\Http\Controllers\ServiceController;
use App\Service;
use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ServiceControllerTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @see ProfileController::index()
     */
    public function testIndex()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('service.index'))
            ->assertOk();
    }
    /**
     * @see ServiceController::create()
     */
    public function testCreate()
    {
        $this->actingAs(factory(User::class)->create())
            ->get(route('service.create'))
            ->assertOk();
    }

    /**
     * @see ServiceController::store()
     */
    public function testStore()
    {
        $requestData = factory(Service::class)
            ->create()
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store'), $requestData)
            ->assertSessionHasNoErrors();
    }

    /**
     * @see ServiceController::show()
     */
    public function testShow()
    {
        $requestData = factory(Service::class)->create();
        $this->actingAs(factory(User::class)->create())
            ->get(route('service.show', ['service' => $requestData]))
            ->assertOk()
            ->assertViewHas('service');
    }

    /**
     * @see ServiceController::edit()
     */
    public function testEdit()
    {
        $requestData = factory(Service::class)->create();
        $this->actingAs(factory(User::class)->create())
            ->get(route('service.show', ['service' => $requestData]))
            ->assertOk()
            ->assertViewHas('service');
    }

    /**
     * @see ServiceController::update()
     */
    public function testUpdate()
    {
        $service = factory(Service::class)->create();
        $requestData = factory(Service::class)->make()->toArray();

        $this->actingAs(factory(User::class)->create())
            ->patch(route('service.update', ['service' => $service]), $requestData)
            ->assertRedirect(route('service.edit', ['service' => $service]));

        $this->assertEquals($requestData['title'], Service::first()->title);
    }

    /**
     * @see ServiceController::destroy()
     */
    public function testDestroy()
    {
        $service = factory(Service::class)->create();
        $this->actingAs(factory(User::class)->create())
            ->delete(route('service.destroy', ['service' => $service]))
            ->assertRedirect(route('index'));

        $this->assertCount(0, Service::all());
    }

    /** @test */
    public function testCategoryValidation()
    {
        $request = factory(Service::class)
            ->create(['category' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('category');
    }

    /** @test */
    public function testTitleValidation()
    {
        $request = factory(Service::class)
            ->create(['title' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('title');

        $request['title'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('title');

    }

    /** @test */
    public function testSubscriptionValidation()
    {
        $request = factory(Service::class)
            ->create(['subscription' => null])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasNoErrors();

        $request['subscription'] = 'S';

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('subscription');

    }

    /** @test */
    public function testDocumentsValidation()
    {
        $request = factory(Service::class)
            ->create(['documents' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('documents');
    }

    /** @test */
    public function testAgeValidation()
    {
        $request = factory(Service::class)
            ->create()
            ->toArray();

        $request['age'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('age');

    }

    /** @test */
    public function testPriceValidation()
    {
        $request = factory(Service::class)
            ->create()
            ->toArray();

        $request['price'] = 'String';

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('price');

    }

    /** @test */
    public function testDurationValidation()
    {
        $request = factory(Service::class)
            ->create(['duration' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('duration');
    }

    /** @test */
    public function testDisabledValidation()
    {
        $request = factory(Service::class)
            ->create(['disabled' => ''])
            ->toArray();

        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('disabled');

        $request['disabled'] = 'String';
        $this->actingAs(factory(User::class)->create())
            ->post(route('service.store', $request))
            ->assertSessionHasErrors('disabled');
    }
}
