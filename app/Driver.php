<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Driver extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $casts = [
        'phones' => 'array'
    ];

    public function orders() {
        return $this->hasMany(Order::class);
    }
}
