<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Profile
 *
 * @property int $id
 * @property string $surname
 * @property string $name
 * @property string|null $patronymic
 * @property string|null $image
 * @property string $birthday
 * @property string $birthplace
 * @property string $address
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_issued
 * @property string $passport_date
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Profile onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereBirthplace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePassportDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePassportSeries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Profile withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Profile withoutTrashed()
 * @mixin \Eloquent
 * @property array $phones
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Profile wherePhones($value)
 */
class Profile extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    protected $casts = [
      'phones' => 'array'
    ];

    public function user() {
        return $this->hasOne(User::class);
    }
}
