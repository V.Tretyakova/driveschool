<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Service
 *
 * @property int $id
 * @property string $category
 * @property string $title
 * @property string|null $subscription
 * @property string $documents
 * @property int $age
 * @property int $price
 * @property string $duration
 * @property bool $disabled
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Service onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereAge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDisabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDocuments($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereSubscription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Service whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Service withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Service withoutTrashed()
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Order[] $orders
 */
class Service extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function orders() {
        return $this->hasMany(Order::class);
    }
}
