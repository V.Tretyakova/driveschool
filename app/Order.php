<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * App\Order
 *
 * @property int $id
 * @property string|null $receipt
 * @property string|null $reserved
 * @property string|null $confirmed
 * @property string $surname
 * @property string $name
 * @property string|null $patronymic
 * @property string $birthday
 * @property string $birthplace
 * @property string $address
 * @property string $passport_series
 * @property string $passport_number
 * @property string $passport_issued
 * @property string $passport_date
 * @property array $phones
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $user_id
 * @property int $service_id
 * @property-read \App\Service $service
 * @property-read \App\User $user
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Order onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereBirthplace($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereConfirmed($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePassportDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePassportIssued($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePassportNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePassportSeries($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePhones($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereReceipt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereReserved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereSurname($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUserId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Order withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Order withoutTrashed()
 * @mixin \Eloquent
 */
class Order extends Model
{
    use SoftDeletes;

    protected $guarded = [];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function service() {
        return $this->belongsTo(Service::class);
    }

    public function driver() {
        return $this->belongsTo(Driver::class);
    }
}
