<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Requests\StoreDriverRequest;
use App\Http\Requests\StoreOrderRequest;
use App\Http\Requests\UpdateDriverRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Order;
use App\Service;
use DB;
use Illuminate\Http\Response;
use Illuminate\Pagination\LengthAwarePaginator;
use Image;
use Validator;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LengthAwarePaginator
     */
    public function index()
    {
        return response(Order::paginate(25));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $profile = auth()->user()->profile;
        $services = Service::all();
        return view('order.create', compact(['profile', 'services']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreOrderRequest $storeOrderRequest
     * @return void
     */
    public function store(StoreOrderRequest $storeOrderRequest)
    {
        DB::beginTransaction();

        $storeDriverRequest = (new StoreDriverRequest([], $storeOrderRequest->all(), [], [], $storeOrderRequest->allFiles(), [], []))
            ->setValidator(Validator::make($storeOrderRequest->all(), (new StoreDriverRequest)->rules()));

        $driver = (new DriverController())->store($storeDriverRequest);

        $order = Order::create(array_merge($storeOrderRequest->validated(), ['user_id' => auth()->user()->id,
            'driver_id' => $driver->id]));

        $this->storeImage($order);

        DB::commit();

        return redirect(route('order.edit', compact('order')));
    }

    /**
     * Display the specified resource.
     *
     * @param Order $order
     * @return Response
     */
    public function show(Order $order)
    {
        $driver = $order->driver()->firstOrFail();
        $services = Service::all();
        return view('order.show', compact(['order', 'driver', 'services']));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Order $order
     * @return void
     */
    public function edit(Order $order)
    {
        $driver = $order->driver()->firstOrFail();
        $services = Service::all();
        return view('order.edit', compact(['order', 'driver', 'services']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateOrderRequest $updateOrderRequest
     * @param Order $order
     * @return void
     * @throws \Exception
     */
    public function update(UpdateOrderRequest $updateOrderRequest, Order $order)
    {
        DB::beginTransaction();

        $updateDriverRequest = (new UpdateDriverRequest([], $updateOrderRequest->all(), [], [], $updateOrderRequest->allFiles(), [], []))
            ->setValidator(Validator::make($updateOrderRequest->all(), (new UpdateDriverRequest)->rules()));

        $order->driver()->update($updateDriverRequest->validated());

        $order->update($updateOrderRequest->validated());

        $this->storeImage($order);

        DB::commit();

        return redirect(route('order.edit', compact('order')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Order $order
     * @return Response
     * @throws \Exception
     */
    public function destroy(Order $order)
    {
        //
        $order->delete();
        return redirect(route('index'));
    }

    private function storeImage($order)
    {
        if (request()->has('receipt')) {
            $order->update([
                'receipt' => request()->receipt->store('uploads/receipt', 'public'),
            ]);
            Image::make(public_path('storage/' . $order->receipt))
                ->fit(300, 300, null, 'center')->save();
        }
    }
}
