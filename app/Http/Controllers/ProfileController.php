<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProfileRequest;
use App\Http\Requests\UpdateProfileRequest;
use App\Profile;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Response;
use Image;

class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return LengthAwarePaginator
     */
    public function index()
    {
        return response(Profile::paginate(25));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('profile.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreProfileRequest $storeProfileRequest
     * @return void
     */
    public function store(StoreProfileRequest $storeProfileRequest)
    {
        $profile = Profile::create($storeProfileRequest->validated());
        auth()->user()->profile()->associate($profile);
        $this->storeImage($profile);
        return response($profile);
    }

    /**
     * Display the specified resource.
     *
     * @param Profile $profile
     * @return Response
     */
    public function show(Profile $profile)
    {
        return view('profile.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Profile $profile
     * @return void
     */
    public function edit(Profile $profile)
    {
        return view('profile.edit', compact('profile'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateProfileRequest $updateProfileRequest
     * @param Profile $profile
     * @return void
     */
    public function update(UpdateProfileRequest $updateProfileRequest, Profile $profile)
    {
        $profile->update($updateProfileRequest->validated());
        $this->storeImage($profile);
        return redirect(route('profile.edit', compact('profile')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Profile $profile
     * @return Response
     * @throws \Exception
     */
    public function destroy(Profile $profile)
    {
        //
        $profile->delete();
        return redirect(route('index'));
    }

    private function storeImage($profile)
    {
        if (request()->has('image')) {
            $profile->update([
                'image' => request()->image->store('uploads', 'public'),
            ]);
            Image::make(public_path('storage/' . $profile->image))
                ->fit(300, 300, null, 'center')->save();
        }
    }
}
