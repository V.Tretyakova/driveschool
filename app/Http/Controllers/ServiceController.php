<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreServiceRequest;
use App\Http\Requests\UpdateServiceRequest;
use App\Service;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $services = Service::paginate(25);
        return response($services);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreServiceRequest $storeServiceRequest
     * @return \Illuminate\Http\Response
     */
    public function store(StoreServiceRequest $storeServiceRequest)
    {
        //
        $service = new Service($storeServiceRequest->validated());
        $service->save();
        return response($service);
    }

    /**
     * Display the specified resource.
     *
     * @param  Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
        return view('service.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        //
        return view('service.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateServiceRequest $updateServiceRequest
     * @param Service $service
     * @return void
     */
    public function update(UpdateServiceRequest $updateServiceRequest, Service $service)
    {
        //
        $service->update($updateServiceRequest->validated());
        return redirect('service/' . $service->id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Service $service
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Service $service)
    {
        //
        $service->delete();
        return redirect('/');
    }
}
