<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Http\Requests\StoreDriverRequest;
use App\Http\Requests\UpdateDriverRequest;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(Driver::paginate(25));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $profile = auth()->user()->profile;
        return view('driver.create', compact(['driver','profile']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDriverRequest $storeDriverRequest
     * @return Driver|\Illuminate\Database\Eloquent\Model
     */
    public function store(StoreDriverRequest $storeDriverRequest)
    {
        return Driver::create($storeDriverRequest->validated());
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function show(Driver $driver)
    {
        return view('driver.show', compact('driver'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        return view('driver.edit', compact('driver'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDriverRequest $updateDriverRequest
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDriverRequest $updateDriverRequest, Driver $driver)
    {
        $driver->update($updateDriverRequest->validated());
        return redirect(route('driver.edit', compact('driver')));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Driver $driver)
    {
        $driver->delete();
        return redirect(route('index'));
    }


}
