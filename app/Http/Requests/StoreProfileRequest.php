<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'surname' => 'required|min:2',
            'name' => 'required|min:2',
            'patronymic' => 'nullable|min:2',
            'image' => 'sometimes|file|image|dimensions:min_width=80,min_height=80,max_width=4096,max_height=4096',
            'birthday' => 'required|date',
            'birthplace' => 'required',
            'address' => 'required',
            'passport_series' => 'required',
            'passport_number' => 'required|numeric',
            'passport_issued' => 'required',
            'passport_date' => 'required|date',
            'phones' => 'required|array'
        ];
    }
}
