<?php

namespace App\Http\Requests;

use App\Rules\CheckboxRule;
use Illuminate\Foundation\Http\FormRequest;

class StoreServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'category' => 'required',
            'title' => 'required|min:2',
            'subscription' => 'nullable|min:2',
            'documents' => 'required',
            'age' => 'required|numeric',
            'price' => 'required|numeric',
            'duration' => 'required',
            'disabled' => ['required', new CheckboxRule()]
        ];
    }
}
