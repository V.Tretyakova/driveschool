const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.js('resources/js/app.js', 'public/js');
mix.js('resources/plugins/fresco-2.2.5-light/fresco.js', 'public/js').version();
mix.js('resources/js/fileBrowser.js', 'public/js').version();
mix.js('resources/js/gallery.js', 'public/js').version();
mix.js('resources/js/navbar.js', 'public/js').version();
mix.js('resources/js/yandexMap.js', 'public/js').version();
mix.js('resources/js/dropdown.js', 'public/js').version();
mix.js('resources/js/docsTable.js', 'public/js').version();
mix.js('resources/js/phoneMask.js', 'public/js').version();
mix.js('resources/js/phoneInput.js', 'public/js').version();
mix.js('node_modules/@fortawesome/fontawesome-free/js/all.js', 'public/js/fontawesome-free/all.js');
mix.js('resources/js/profile/show.js', 'public/js/profile').version();
mix.js('resources/js/service/show.js', 'public/js/service').version();
mix.js('resources/js/service/disabledCheckbox.js', 'public/js/service').version();
mix.js('resources/js/driver/show.js', 'public/js/driver').version();
mix.js('resources/js/order/show.js', 'public/js/order').version();

mix.sass('resources/sass/app.scss', 'public/css').version();

mix.webpackConfig(webpack => {
    return {
        plugins: [
            new webpack.ProvidePlugin({
                $: 'jquery',
                jQuery: 'jquery',
                Inputmask: 'inputmask',
                'window.Inputmask': 'inputmask',
                '$.inputmask':'inputmask'
            })
        ]
    };
});
